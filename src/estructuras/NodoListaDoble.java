package estructuras;


/**
 * Nodo de una lista doblemente encadenada
 * @author jh.lake
 * @param <T>
 */
public class NodoListaDoble<T> extends NodoListaSencilla<T>
{

    /**
     * Nodo anterior
     */
    private NodoListaDoble<T> anterior;
    
    /**
     * Construye un nuevo nodo doblemente encadenado
     * @param elem el elemento que almacena el nodo
     */
    public NodoListaDoble (T elem)
    {
        super(elem);
        anterior = null;
    }
    
    /**
     * Devuelve el nodo anterior
     * @return anterior
     */
    public NodoListaDoble<T> darAnterior()
    {
        return anterior;
    }

    /**
     * Modifica el nodo anterior 
     * @param ant el nuevo nodo anteiror.
     */
    public void cambiarAnterior( NodoListaDoble<T>  ant )
    {
        anterior = ant;
    }
    
}
