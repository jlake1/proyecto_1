package estructuras;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Clase que representa una lista sencillamente encadenada
 * @author jh.lake
 * @param <T>
 */
public class ListaSencillamenteEncadenada<T>
{
    
    /**
     * Primer nodo de la lista
     */
    protected NodoListaSencilla<T> primero;
    
    /**
	 * Se construye una nueva lista cuyo primer nodo  guardar� al elemento que llega por par�mentro
	 * @param nPrimero el elemento a guardar en el primer nodo
	 * @throws NullPointerException si el elemento recibido es nulo
	 */
	public ListaSencillamenteEncadenada(T nPrimero)
	{
	    if(nPrimero == null)
	    {
	        throw new NullPointerException();
	    }
	    primero = new NodoListaSencilla<T>( nPrimero );
	}

	/**
	 * Construye una lista vacia
	 * post: se ha inicializado el primer nodo en null
	 */
	public ListaSencillamenteEncadenada()
	{
	    primero = null;
	}

    /**
     * Indica el tama�o de la lista
     * @return cantidad de nodos en la lista
     */
    public int size( )
    {
        int size = 0;
        NodoListaSencilla<T> nodo = primero;
        
        while( nodo != null)
        {
            size++;
            nodo = nodo.darSiguiente( );
        }
        return size;
    }
    
    /**
     * Reemplaza el elemento de la posici�n por el elemento que llega por par�metro
     * @param pos la posici�n en la que se desea reeemplazar el elemento
     * @param elem el nuevo elemento que se desea poner
     * @return el elemento quitado de la posici�n
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T set( int pos, T elem )
    {
        if(pos<0)
            throw new ArrayIndexOutOfBoundsException( );
        boolean found = false;
        int count = 0;
        NodoListaSencilla<T> ele = primero;
        if(primero == null)
            return null;
        T obj = ele.darElemento( );
        while(!found)
        {
            if(ele == null)
                throw new ArrayIndexOutOfBoundsException( );
            if( count == pos)
            {
                found = true;
                obj = ele.darElemento( );
                ele.cambiarElemento( elem );
            }
            count++;
            ele = ele.darSiguiente( );
        }
        return obj;
            
       
    }
    
    /**
     * Borra de la lista todos los elementos en la colecci�n por par�metro
     * @param coleccion la colecci�n de elmentos que se desea eliminar. coleccion != null
     * @return true en caso que se elimine al menos un elemento o false en caso contrario
     */
     public boolean removeAll( Collection<?> coleccion )
     {
         boolean modificado = false;
         for( Object objeto : coleccion )
         {
             modificado |= remove( objeto );
         }
         return modificado;
     }

	/**
      * Indica la �ltima posici�n donde aparece el objeto por par�metro en la lista
      * @param objeto el objeto buscado en la lista. objeto != null
      * @return la �ltima posici�n del objeto en la lista o -1 en caso que no exista
      */
     public int lastIndexOf( Object objeto )
     {
         return indexOf( objeto );
     }
     
     /**
      * Devuelve un iterador sobre la lista
      * El iterador empieza en el primer elemento
      * @return un nuevo iterador sobre la lista
      */
     public Iterator<T> iterator( )
     {
         return new IteradorSencillo<T>( primero );
     }
     
     /**
      * Indica si la lista est� vacia
      * @return true si la lista est� vacia o false en caso contrario
      */
     public boolean isEmpty( )
     {
         return primero == null;
     }
     
     /**
      * Indica la posici�n del objeto que llega por par�metro en la lista
      * @param objeto el objeto que se desea buscar en la lista. objeto != null
      * @return la posici�n del objeto o -1 en caso que no se encuentre en la lista
      */
     public int indexOf( Object objeto )
     {
         int index = -1;
         boolean found = false;
         NodoListaSencilla<T> ele =primero;
         for( int i = 0; !found; i++)
         {
             if(ele == null)
                 break;
             if(ele.darElemento( ).equals( objeto ))
                 index = i;
             ele = ele.darSiguiente( );
         }
         return index;
       
     }
     
     /**
      * Devuelve el elemento de la posici�n dada
      * @param pos la posici�n  buscada
      * @return el elemento en la posici�n dada 
      * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
      */
     public T get( int pos )
     {
         if(pos<0)
             throw new ArrayIndexOutOfBoundsException( );
         boolean found = false;
         int count = 0;
         NodoListaSencilla<T> ele = primero;
         if(primero == null)
             return null;
         while (!found)
         {
             if(ele == null )
                 throw new ArrayIndexOutOfBoundsException( );
             if(count == pos)
                  break;
             count++;
             ele = ele.darSiguiente( );
         }
         return ele.darElemento( );
       
     }
     
     /**
      * Devuelve el nodo de la posici�n dada
      * @param pos la posici�n  buscada
      * @return el nodo en la posici�n dada 
      * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
      */
     public NodoListaSencilla<T> getNodo( int pos )
     {
         if(pos<0)
             throw new ArrayIndexOutOfBoundsException( );
         boolean found = false;
         NodoListaSencilla<T> ele = primero;
         NodoListaSencilla<T> ans = null;
         for(int i=0; !found; i++)
         {
             if(ele == null)
                 throw new ArrayIndexOutOfBoundsException( );
             if(i == pos)
             {
                 found = true;
                 ans = ele;
             }
             ele = ele.darSiguiente( );
         }
         return ans;
        
     }
     
     /**
      * Indica si la lista contiene todos los objetos de la colecci�n dada
      * @param coleccion la colecci�n de objetos que se desea buscar. coleccion !=null
      * @return true en caso que todos los objetos est�n en la lista o false en caso contrario
      */
     public boolean containsAll( Collection<?> coleccion )
     {
         boolean contiene = true;
         for( Object objeto : coleccion)
         {
             contiene &= contains( objeto );
         }
         return contiene;
     }
     
     /**
      * Indica si la lista contiene el objeto indicado
      * @param objeto el objeto que se desea buscar en la lista. objeto != null
      * @return true en caso que el objeto est� en la lista o false en caso contrario
      */
     public boolean contains( Object objeto )
     {
    	 boolean contiene = false;
    	 NodoListaSencilla<T> nodo = primero;
    	 while( nodo!= null && !contiene)
    	 {
    		 if(nodo.darElemento( ).equals( objeto ))
    		 {
    			 contiene = true;
    		 }
    		 nodo = nodo.darSiguiente( );
    	 }
    	 return contiene;
     }

      /**
       * Borra todos los elementos de la lista
       * post: el primer elemento es nulo
       */
      public void clear( )
      {
          primero = null;
          
      }
      
      /**
       * Agrega todos los elementos de la colecci�n a la lista a partir de la posici�n indicada
       * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
       * @param pos la posici�n a partir de la cual se desean agregar los elementos
       * @param coleccion la colecci�n de elementos que se desea agregar
       * @return true si al menos uno de los elementos se agrega o false en caso contrario
       * @throws NullPointerException Si alguno de los elementos que se quiere agregar es null
       * @throws IndexOutOfBoundsException si indice < 0 o indice > size()
       */
      public boolean addAll( int pos, Collection<? extends T> coleccion )
      {

          int size0 = size( );
          for( T t : coleccion )
          {
             add( pos, t );
             pos++;
          }
          return size( ) > size0;
      }
      
      /**
       * Agrega a la lista todos los elementos de la colecci�n
       * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
       * @param coleccion la colecci�n de elementos que se desea agregar
       * @return true en caso que se agregue al menos un elemento false en caso contrario
       * @throws NullPointerException si alguno de los elementos que se desea agregar es null
       */
      public boolean addAll( Collection<? extends T> coleccion )
      {
          boolean modificado = false;
          for( T t : coleccion )
          {
              modificado |= add(t);
          }
          return modificado;
      }


    /**
     * Agrega un elemento al final de la lista
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param elem el elemento que se desea agregar.
     * @return true en caso que se agregue el elemento o false en caso contrario. 
     * @throws NullPointerException si el elemento es nulo
     */
    public boolean add( T elem )throws NullPointerException
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }
        
        boolean agregado = false;
        if(primero == null)
        {
            primero = new NodoListaSencilla<T>( elem );
            agregado = true;
        }
        else
        {
            NodoListaSencilla<T> n = primero;
            boolean existe = false;
            while(  n.darSiguiente( ) != null && !existe)
            {
                if(n.darElemento( ).equals( elem ))
                {
                    existe = true;
                }
                n = n.darSiguiente( );
            }
            if(!n.darElemento( ).equals( elem ))
            {
                n.cambiarSiguiente( new NodoListaSencilla<T>( elem ) );
            }
            
        }
        
        return agregado;
    }

    /**
     * Agrega un elemento en la posici�n dada de la lista. Todos los elementos siguientes se desplazan
     * Un elemento no se agrega si la lista ya tiene un elemento con el mismo id
     * @param pos la posici�n donde se desea agregar. Si pos es igual al tama�o de la lista se agrega al final
     * @param elem el elemento que se desea agregar
     * @throws IndexOutOfBoundsException si el inidice es < 0 o > size()
     * @throws NullPointerException Si el elemento que se quiere agregar es null.
     */
    public void add( int pos, T elem )
    {
        if(elem == null)
        {
            throw new NullPointerException( );
        }
        NodoListaSencilla<T> nuevo = new NodoListaSencilla<T>( elem );
        if(!contains( elem ))
        {
            
            if(pos == 0)
            {
                nuevo.cambiarSiguiente( primero );
                primero = nuevo;
            }
            else
            {
                NodoListaSencilla<T> n = primero;
                int posActual = 0;
                while( posActual < (pos-1) && n != null )
                {
                    posActual++;
                    n = n.darSiguiente( );
                }
                if(posActual != (pos-1))
                {
                    throw new IndexOutOfBoundsException( );
                }
                nuevo.cambiarSiguiente( n.darSiguiente( ) );
                n.cambiarSiguiente( nuevo );
            }
        }
        
    }

    /**
     * Elimina el nodo que contiene al objeto que llega por par�metro
     * @param objeto el objeto que se desea eliminar. objeto != null
     * @return true en caso que exista el objeto y se pueda eliminar o false en caso contrario
     */
    public boolean remove( Object objeto )
    {
        boolean rem = false;
        NodoListaSencilla<T> ele = primero;
        if(ele == null)
            return false;
        if(ele.darElemento( ).equals( objeto ))
        {
            primero = ele.darSiguiente( );
            rem = true;
        }
        while(!rem)
        {
            NodoListaSencilla<T> sig = ele.darSiguiente( );
            if(sig == null)
                break;
            if(sig.darElemento( ).equals( objeto ))
            {
                ele.cambiarSiguiente( sig.darSiguiente( ) );
                rem = true;
            }
            ele = sig;
        }
        return rem;
       
    }

    /**
     * Elimina el nodo en la posici�n por par�metro
     * @param pos la posici�n que se desea eliminar
     * @return el elemento eliminado
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T remove( int pos )
    {
        if(pos <0)
            throw new ArrayIndexOutOfBoundsException( );
        int count = 0;
        boolean found = false;
        NodoListaSencilla<T> ele =primero;
        if(ele == null)
            return null;
        if(pos == 0)
        {
            primero = ele.darSiguiente( );
            return ele.darElemento( );
        }
        T obj = null;
        while(!found)
        {
            NodoListaSencilla<T> sig = ele.darSiguiente( );
            if(sig == null)
                throw new ArrayIndexOutOfBoundsException( );
            if(count +1 == pos) 
            {
                obj = sig.darElemento( );
                found = true;
                ele.cambiarSiguiente( sig.darSiguiente( ) );
            }
            ele = sig;
            count++;
        }
        return obj;
        
    }

    /**
     * Deja en la lista solo los elementos que est�n en la colecci�n que llega por par�metro
     * @param coleccion la colecci�n de elementos a mantener. coleccion != null
     * @return true en caso que se modifique (eliminaci�n) la lista o false en caso contrario
     */
    public boolean retainAll( Collection<?> coleccion )
    {
        boolean change = false;
        NodoListaSencilla<T> ele =primero;
        if(ele == null)
            return false;
        NodoListaSencilla<T> sig = ele.darSiguiente( );
        if(!(coleccion.contains( ele.darElemento() )))
        {
            primero = sig;
            ele = sig;
            sig = sig.darSiguiente( );
        }
        while(sig!=null)
        {
            if(!(coleccion.contains(ele.darElemento( ) )))
            {
                remove(ele.darElemento( ));
                change = true;
            }
        }
        return change;
       
    }
    
    /**
     * Crea una lista con los elementos de la lista entre las posiciones dadas
     * @param inicio la posici�n del primer elemento de la sublista. Se incluye en la sublista
     * @param fin la posici�n del �tlimo elemento de la sublista. Se excluye en la sublista
     * @return una lista con los elementos entre las posiciones dadas
     * @throws IndexOutOfBoundsException Si inicio < 0 o fin >= size() o fin < inicio
     */
    public ListaSencillamenteEncadenada<T> subList( int inicio, int fin )
    {
        if(inicio < 0 || fin< inicio)
            throw new IndexOutOfBoundsException( );
        ListaSencillamenteEncadenada<T> ans = new ListaSencillamenteEncadenada<T>( );
        int count = 0;
        NodoListaSencilla<T> ele = primero;
        if(ele == null)
            return null;
        while(count < fin )
        {
            if(ele==null)
                throw new IndexOutOfBoundsException( );
            if(count >= inicio)
            {
                ans.add( ele.darElemento( ) );
            }
            count++;
            ele = ele.darSiguiente( );
        }
        return ans;
    }

}
