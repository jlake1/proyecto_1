package estructuras;

public class Queue<T> {
	/**
	 * Atributo que representa el primer elemento del stack.
	 */
	private NodoListaSencilla<T> primero;
	
	/**
	 * Crea una cola vacía.
	 */
	public Queue()
	{
		primero = null;
	}
	
    /** 
     * Pone el objeto dado por parámetro en la cola.
     * @param	object	El objeto por agregar al final de la cola.
     */
    public synchronized void put(T object) {
    	if(object == null)
        {
            throw new NullPointerException( );
        }
        if(primero == null)
        {
            primero = new NodoListaSencilla<T>( object );
        }
        else
        {
            NodoListaSencilla<T> n = primero;
            boolean existe = false;
            while(  n.darSiguiente( ) != null && !existe)
            {
                if(n.darElemento( ).equals( object ))
                {
                    existe = true;
                }
                n = n.darSiguiente( );
            }
            if(!n.darElemento( ).equals( object ))
            {
                n.cambiarSiguiente( new NodoListaSencilla<T>( object ) );
            }
        }
    }

    /**
     * Toma el primer elemento en la cola, y lo devuelve.
     * <b> pos: </b> El elemento que estaba de primero se elimina.
     * @return El elemento si lo encuentra, null si no.
     */
    public synchronized T pull() {
    	T obj = peek();
    	if(obj != null)
    		primero = primero.darSiguiente();
    	return obj;
    }

    /**
     * Revisa el primer elemento de la cola, y lo devuelve.
     * @return El elemento en la primera posición de la cola. Null si está vacía.
     */
    public T peek() {
    	if(isEmpty())
    		return null;
    	return primero.darElemento();
    }

    /**
     * Verifica si la cola está vacía.
     * @return true si la cola está vacía, false de lo contrario.
     */
    public boolean isEmpty() {
    	return (primero == null);
    }

	public int size() {
		int size = 0;
        NodoListaSencilla<T> nodo = primero;
        while( nodo != null)
        {
            size++;
            nodo = nodo.darSiguiente( );
        }
        return size;
	}
	
	/**
     * Devuelve el elemento de la posici�n dada
     * @param pos la posici�n  buscada
     * @return el elemento en la posici�n dada 
     * @throws IndexOutOfBoundsException si pos < 0 o pos >= size()
     */
    public T get( int pos )
    {
        if(pos<0)
            throw new ArrayIndexOutOfBoundsException( );
        boolean found = false;
        int count = 0;
        NodoListaSencilla<T> ele = primero;
        if(primero == null)
            return null;
        while (!found)
        {
            if(ele == null )
                throw new ArrayIndexOutOfBoundsException( );
            if(count == pos)
                 break;
            count++;
            ele = ele.darSiguiente( );
        }
        return ele.darElemento( );
    }
}