package interfaz;

/**
 * Clase principal del proyecto de colisiones
 */
public class Main {

	/**
	 * Constante que representa el separador de los archivos csv.
	 */
	public static final String separadorCSV = ",";
	
	/**
	 * Constante que representa el separador de los archivos JSON.
	 */
	public static final String separadorJSON = ":";
	
	/**
	 * 
	 */
	public static final String separadorCol = " ";
	
	
	
	
	//----------------------------------------------
	//  Main
	//----------------------------------------------
	
	/**
	 * Main
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
			new Main();
		}
		catch(Exception e)
		{
			// TODO Auto-generated method stub
		}
	}

}
