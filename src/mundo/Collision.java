package mundo;

/**
 * Clase que representa todo tipo de colisiones.
 * @author jlake
 */
public abstract class Collision {

/**
 * Gravedad de una colisión.
 */
private enum Gravedad {
	FATAL,
	SLIGHT,
	SERIOUS;
}
	//-----------------------------------------
	//Atributos
	//-----------------------------------------

	/**
	 * Atributo que representa la gravedad de una colisión.
	 */
	private Gravedad tipo;
	
	/**
	 * Atributo que representa el nombre del área donde ocurrió la colisión.
	 */
	private String nombre;
	
	//-----------------------------------------
	// Métodos
	//-----------------------------------------
	
	
}
